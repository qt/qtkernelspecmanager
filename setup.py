from setuptools import setup, find_packages

setup(
    name='qtkernelspecmanager',
    version='0.1.0',
    packages=find_packages(),
    url='https://gitlab.kwant-project.org/qt/qtkernelspecmanager',
    license='MIT',
    author='Quantum Tinkerer group',
    author_email='admin@quantumtinkerer.group',
    description='A customization of CondaKernelSpecManager, '
                'that is used in Quantum Tinkerer group',
    install_requires=['nb_conda_kernels']
)
